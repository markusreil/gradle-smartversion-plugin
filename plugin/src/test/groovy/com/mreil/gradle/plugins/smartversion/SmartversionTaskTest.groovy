package com.mreil.gradle.plugins.smartversion

import com.mreil.gradle.plugins.buildinfo.model.BuildInfo
import com.mreil.gradle.plugins.buildinfo.model.BuildinfoExtension
import org.gradle.api.Action
import org.gradle.api.Project
import org.gradle.api.Task
import org.gradle.testfixtures.ProjectBuilder
import spock.lang.Shared
import spock.lang.Specification

class SmartversionTaskTest extends Specification {
    @Shared
    Task task

    @Shared
    Project project

    @Shared
    SmartversionExtension ext

    @Shared
    BuildinfoExtension buildinfoExtension

    def setup() {
        project = ProjectBuilder.builder().build()
        project.plugins.apply(SmartversionPlugin.ID)
        ext = project.smartversion
        buildinfoExtension = project.buildinfo
        task = project.tasks.getByName(SmartversionTask.NAME)
    }


    def "Fixed version stays unchanged"() {
        when:
        project.version = "1.2"
        runTask()

        then:
        ext.version == "1.2"
    }

    def "Snapshot version is returned"() {
        when:
        runTask()

        then:
        project.version == "${shortDate()}-revision".toString()
    }

    def "Snapshot version is returned with suffix"() {
        when:
        project.smartversion {
            snapshotExtension = true
        }
        runTask()

        then:
        project.version == "${shortDate()}-revision-SNAPSHOT".toString()
    }

    def "Whole release version is returned"() {
        when:
        buildinfoExtension.buildInfo = new BuildInfo(null, null, "release")
        ext.releaseBranchPattern = "rel.*"
        runTask()

        then:
        ext.release
        project.version == "release-${shortDate()}".toString()
    }

    def "Matched group of release version is returned"() {
        when:
        buildinfoExtension.buildInfo = new BuildInfo(null, null, "release/rel-2.1")
        ext.releaseBranchPattern = "release/rel-(.*)"
        runTask()

        then:
        project.version == "2.1-${shortDate()}".toString()
    }

    def runTask() {
        task.actions.each { Action a ->
            a.execute(task)
        }
    }

    private String shortDate() {
        SmartversionTask.extractDate(buildinfoExtension.buildInfo.date)
    }

}
