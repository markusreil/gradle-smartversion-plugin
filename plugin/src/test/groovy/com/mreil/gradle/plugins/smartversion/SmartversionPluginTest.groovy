package com.mreil.gradle.plugins.smartversion
import com.mreil.gradle.plugins.buildinfo.BuildinfoPlugin
import org.gradle.api.Project
import org.gradle.testfixtures.ProjectBuilder
import spock.lang.Shared
import spock.lang.Specification

class SmartversionPluginTest extends Specification {
    @Shared
    Project project

    def setup() {
        project = ProjectBuilder.builder().build()
        project.model {
            smartversion {
                snapshotExtension = true
            }
            person {
                firstName = "Fuge"
            }
        }
        project.apply(plugin: SmartversionPlugin)
    }

    def "project has extension"() {
        when:
        project.apply(plugin: SmartversionPlugin)

        then:
        project.extensions.each {
            println it
        }
        project.extensions.getByName("smartversion")
    }

    def "buildinfo plugin is also applied"() {
        when:
        project.plugins.apply(SmartversionPlugin.ID)

        then:
        project.plugins.hasPlugin(BuildinfoPlugin.ID)
    }

    def "task gets run automatically"() {
        when:
        project.plugins.apply("java")
        project.plugins.apply(SmartversionPlugin.ID)

        then:
        project.gradle.startParameter.taskNames == [SmartversionTask.NAME]
    }
}
