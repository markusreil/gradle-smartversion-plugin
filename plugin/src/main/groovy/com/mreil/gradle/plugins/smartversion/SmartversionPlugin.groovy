package com.mreil.gradle.plugins.smartversion

import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.model.RuleSource

class SmartversionPlugin extends RuleSource implements Plugin<Project> {
    public static final String ID = 'com.mreil.smartversion'

    public void apply(Project project) {
        project.extensions.create("smartversion", SmartversionExtension)

        if (!project.plugins.hasPlugin("com.mreil.buildinfo")) {
            project.plugins.apply("com.mreil.buildinfo")
        }

        wireTask(project)
    }

    void wireTask(Project project) {
        def task = project.tasks.create("smartversion", SmartversionTask)

        // always run this task as the first task
        project.gradle.startParameter.taskNames = [task.name] + project.gradle.startParameter.taskNames
    }

}
