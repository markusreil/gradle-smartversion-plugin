package com.mreil.gradle.plugins.smartversion
import com.mreil.gradle.plugins.buildinfo.model.BuildinfoExtension
import org.gradle.api.DefaultTask
import org.gradle.api.tasks.TaskAction
import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat
import org.joda.time.format.ISODateTimeFormat

import java.util.regex.Matcher
import java.util.regex.Pattern

class SmartversionTask extends DefaultTask {
    public static final String NAME = "smartversion"
    private SmartversionExtension ext
    private BuildinfoExtension buildinfoExtension
    private Pattern branchPattern

    @TaskAction
    public void task() {
        ext = project.extensions.getByName(SmartversionExtension.NAME)
        buildinfoExtension = project.extensions.getByName("buildinfo")

        if (!ext.enabled) {
            return
        }

        if (project.version != 'unspecified') {
            ext.version = project.version
        } else {
            ext.version = checkForReleaseBranch() ?
                    createReleaseVersion() :
                    createSnapshotVersion()
            logger.warn("Setting project version to ${ext.version}")
            project.version = ext.version
        }

    }

    private String createSnapshotVersion() {
        String version = "${extractDate(buildinfoExtension.buildInfo.date)}-${buildinfoExtension.buildInfo.revision}"
        if (ext.snapshotExtension) {
            version = version + "-SNAPSHOT"
        }
        return version
    }

    private String createReleaseVersion() {
        Matcher m = branchPattern.matcher(buildinfoExtension.buildInfo.branch)
        m.find()
        final String branch
        if (m.groupCount() > 0) {
            branch = m.group(1)
        } else {
            branch = m.group(0)
        }

        return "${branch}-${extractDate(buildinfoExtension.buildInfo.date)}"
    }

    static String extractDate(String dateString) {
        DateTime date = ISODateTimeFormat.dateTimeParser().parseDateTime(dateString)
        return DateTimeFormat.forPattern("yyyyMMdd").withZoneUTC().print(date)
    }

    boolean checkForReleaseBranch() {
        String pattern = ext.releaseBranchPattern
        if (!pattern) {
            return false
        }

        branchPattern = Pattern.compile(ext.releaseBranchPattern)
        ext.release = buildinfoExtension.buildInfo.branch.matches(branchPattern)
        return ext.release
    }
}
