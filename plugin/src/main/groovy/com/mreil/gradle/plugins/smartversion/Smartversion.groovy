package com.mreil.gradle.plugins.smartversion

import org.gradle.model.Managed

@Managed
interface Smartversion {
    void setSnapshotExtension(Boolean n)

    Boolean getSnapshotExtension()
}
