package com.mreil.gradle.plugins.smartversion;

public class SmartversionExtension {
    public static final String NAME = "smartversion"

    boolean enabled = true
    String releaseBranchPattern
    boolean snapshotExtension = false

    // not supposed to be set by the user

    boolean release = false
    private String version
    protected void setVersion(String version) {
        this.version = version
    }
    String getVersion() {
        return version
    }
}
