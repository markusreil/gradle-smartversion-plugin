package com.mreil.gradle.plugins.smartversion

import com.google.common.io.Files
import org.codehaus.plexus.util.FileUtils
import org.eclipse.jgit.api.Git
import org.eclipse.jgit.lib.Constants
import org.gradle.tooling.GradleConnector
import org.gradle.tooling.ProjectConnection
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import spock.lang.Specification

class Test extends Specification {
    Logger log = LoggerFactory.getLogger(Test.class)

    def "Run with plugin"() {
        when: "I have a gradle project"
        def projectDir = Files.createTempDir()
        log.error("Creating project dir at:" + projectDir)
        FileUtils.copyDirectory(new File("src/test/projects/simple"),
                projectDir)

        and: "it is a valid git repo"
        Git git = Git.init().setDirectory(projectDir).call();
        git.add().addFilepattern("*.*").call();
        git.commit().setMessage("initial commit").call();

        and: "I run the project"
        ProjectConnection connect = GradleConnector.newConnector()
                .forProjectDirectory(projectDir)
                .useGradleVersion("2.8")
                .connect()
        connect.newBuild().forTasks("assemble").run()

        then: "the buildinfo file will be created"
        File buildinfo = new File(projectDir, "build/buildinfo/META-INF/buildinfo.json")
        buildinfo.exists()
        List<String> content = buildinfo.readLines()
        content.each {println it}

        and: "it will contain the current branch name"
        content.find{it.contains('"branch": "master"')}

        and: "it will contain the current git revision"
        def revision = git.repository.resolve(Constants.HEAD).toObjectId().abbreviate(6).name()
        println "Current git revision: ${revision}"
        content.find{it.contains(revision)}
    }

}
